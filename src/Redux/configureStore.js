// import { createStore,combineReducers} from "redux";
// import {HOTAL_DATA} from "../Shared/hotalData"
// import { COMMENTS } from "../Shared/CommentsData";
// import thunk from "redux-thunk"
// import logger from "redux-logger"
// export const ConfigureStore = () =>{
//    const store = createStore(
//      combineReducers({
//        hotaldata:HOTAL_DATA,
//        comments:COMMENTS
//      }) 
//       // ,applyMiddleware(thunk,logger)
//    );
//   return store;
// }

import {createStore} from 'redux';
import { Reducer, InitialState } from "../Redux/Reducer";

export const ConfigureStore = () => {
    const store = createStore(
        Reducer, // reducer
        InitialState, // our initialState
    );

    return store;
}