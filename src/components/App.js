import React, { Component } from "react";
import Main from "../components/Main";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
// import createStore from "../Redux/configureStore";
import {ConfigureStore}  from "../Redux/configureStore";
// const store = createStore;
const store =ConfigureStore();
// const store =store.getState();
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Main />
        </Router>
      </Provider>
    );
  }
}
export default App;
