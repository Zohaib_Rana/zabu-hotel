import React, { Component } from "react";
// import { CardImgOverlay } from "reactstrap";
// import { ONCLICKCOMMENTS } from "../components/onClick_Comment";
import { Link } from "react-router-dom";
class Hotals extends Component {
  state = {
    imgSrc2: "assests/images/01kara.jpg",
    imgSrc1: "assests/images/02.jpg",
    imgSrc3: "assests/images/02kar.jpg",
  };
  constructor(props) {
    super(props);

    this.state = {
        selectedHotal: null
    }
}

onHotalSelect(dish) {
    this.setState({ selectedHotal: hotal});
}

renderHotal(hotal) {
  if (hotal != null)
      return(
          <Card>
              <CardImg top src={hotal.image} alt={hotal.name} />
              <CardBody>
                <CardTitle>{hotal.name}</CardTitle>
                <CardText>{hotal.description}</CardText>
              </CardBody>
          </Card>
      );
  else
      return(
          <div></div>
      );
}

  render() {
    
    const HOTAL_MENU = this.props.hotalData.map((hotal) => {
      return (
        <div  className="col-12 col-md-5 m-1">
          <Card key={hotal.id}
            onClick={() => this.onHotalSelect(hotal)}>
            <CardImg width="100%" src={hotal.image} alt={hotal.name} />
            <CardImgOverlay>
                <CardTitle>{hotal.name}</CardTitle>
            </CardImgOverlay>
          </Card>
        </div>
      );
  });
  return (
    <div className="container">
        <div className="row">
            {HOTAL_MENU}
        </div>
        <div className="row">
          <div  className="col-12 col-md-5 m-1">
            {this.renderHotal(this.state.selectedHotal)}
          </div>
        </div>
    </div>
);
  }
}
export default Hotals;
