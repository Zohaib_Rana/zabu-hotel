// import React from 'react';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// import { HOTAL_DATA } from "../Shared/hotalData";
// import { COMMENTS } from "../Shared/CommentsData";
import { Card, CardImg, CardImgOverlay,CardTitle,Breadcrumb,BreadcrumbItem} from 'reactstrap';
// import HotalDetail from "../components/hotal_Details";
// import {Loading} from "../components/LoadingComponent";

// function RenderMenuItem ({hotal}) {
//     return (
//         <Card>
//             <Link to={`/hotals${hotal.id}`}>
//             <CardImg width="100%" src={hotal.image} alt={hotal.name} />
//             <CardImgOverlay>
//                 <CardTitle>{hotal.name}</CardTitle>
//             </CardImgOverlay>
//             </Link>
//         </Card>
//     );
// }

// const Menu_Hotal = (props) => {

//     const menu = props.hotaldata.map((hotal) => {
//         return (
//             <div className="col-12 col-md m-1"  key={hotal.id}>
//                 <RenderMenuItem hotal={hotal} onClick={props.onClick} />
//             </div>
//         );
//     });

//     return (
//         <div className="container">
//             <div className="row">
//                 {menu}
//             </div>
//         </div>
//     );
// }

// export default Menu_Hotal;    
import { FadeTransform, Fade, Stagger } from "react-animation-components";
class Menu_Hotal extends Component{
    // constructor(props){
    //     super(props);
    //     this.state = {
        //   hotaldata: HOTAL_DATA,
        //   comments:  COMMENTS,
        //   selected_Hotal: null
    //         };
    //   }
    //   onHotalSelect = (hotalId)  => {
    //     this.setState({ selected_Hotal: hotalId });
    //   }
    // renderHotal(hotal){
    //     if(hotal != null){
    //         return (
    //             <div className="col-12 col-md-5 m-1"> 
    //             <Card>
    //                 <CardBody>      
    //                 <CardImg width="100%" src={hotal.imgSrc} alt={hotal.hotal_Name} />                           
    //                 <CardTitle>{hotal.hotal_name}</CardTitle>
    //                 <CardText>{hotal.Hotal_Description}</CardText> 
    //                 </CardBody>  
    //             </Card>
    //         </div>
    //     );
    //   }
    //     else return(<div></div>);
    // }
 render(){
    const menu =this.props.hotaldata.map((hotaldata) =>{
        return( 
            <div className="col-12 col-md m-1">
                <FadeTransform
                    in
                    transformProps={{
                    exitTransform: "scale(0.5) translateY(-50%)"
                    }}
                >
                <Card
                    key={hotaldata.id} 
                    // onClick={() =>this.onHotalSelect(hotaldata)} 
                        >
                    <Link to={`/hotels/${hotaldata.id}`}>   
                    <CardImg width="100%" src={hotaldata.imgSrc} alt={hotaldata.hotal_Title} />
                    <CardImgOverlay>
                        <CardTitle >{hotaldata.hotal_Title}</CardTitle>
                    </CardImgOverlay>
                    </Link>
                </Card>
            </FadeTransform>
            </div>
        )}
            );     
        return(
            <div className="container">
                <div className="row">  
                    <Breadcrumb>
                        <BreadcrumbItem>
                        <Link to="/hotels">Hotel's</Link>
                        </BreadcrumbItem>
                        <BreadcrumbItem active>hotel's</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Hotel's</h3>
                        <hr />
                    </div>  
                        {menu}
                </div>
            </div>
        );
    }  
 }
export default Menu_Hotal;
