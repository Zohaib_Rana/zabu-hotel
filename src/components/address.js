<div className="container">
  <div className="row row-content">
    <div className="col-12">
      <h3>Location Information</h3>
    </div>
    <div className="col-12 col-sm-4 offset-sm-1">
      <h5>Our Address</h5>
      <address>
        Badami Bagh Ring Road Lahore
        <br />
        Greater Iqbal Park
        <br />
        HONG KONG
        <br />
        <i className="fa fa-phone"></i>: +92 307 682 1057
        <br />
        <i className="fa fa-fax"></i>: +92 307 682 1057
        <br />
        <i className="fa fa-envelope"></i>:{" "}
        <a href="https://mail.google.com/mail/u/2/?tab=wm&ogbl#inbox">
          zabunisa741@mail.com
        </a>
      </address>
    </div>
    <div className="col-12 col-sm-6 offset-sm-1">
      <h5>Map of our Location</h5>
    </div>
    <div className="col-12 col-sm-11 offset-sm-1">
      <div className="btn-group" role="group">
        <a role="button" className="btn btn-primary" href="http://google.com">
          <i className="fa fa-phone"></i> Call
        </a>
        <a role="button" className="btn btn-info" href="http://google.com">
          <i className="fa fa-skype"></i> Skype
        </a>
        <a
          role="button"
          className="btn btn-success"
          href="mailto:confusion@food.net"
        >
          <i className="fa fa-envelope-o"></i> Email
        </a>
      </div>
    </div>
  </div>
</div>;
