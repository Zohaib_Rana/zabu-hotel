import React, { Component } from "react";
import NavBar from "../components/NavBar";
import Menu_Hotal from "../components/Menu_Hotal";
import Home from "../components/Home";
import About from "./about";
import Login from "../components/Login";
import Footer from "../components/Footer";
import Contact from "../components/ContactUs";
import SignIn from "../components/SignIn";
import { Switch, Route, Redirect,withRouter } from "react-router-dom";
import { connect } from "react-redux";
// import {postComment,fetch_Hotals} from '../Redux/actionCreater';
// import { Comments } from "./Comments";
import HotalDetail from "../components/hotal_Details";
import { HOTAL_DATA } from "../Shared/hotalData";
import { COMMENTS } from "../Shared/CommentsData";
  // const mapDispatchToProps = (Dispatch) => ({
  // postComment:(hotalId,rating,author,comment)=> Dispatch(postComment(hotalId,rating,author,comment))
  //  ,fetch_Hotals:() =>{Dispatch(fetch_Hotals())}
  // });
  const mapStateToProp = (state) => {
    return {
    hotaldata: state.hotaldata,
    comments:state.comments,
    selected_Hotal: null
  };
};

class Main extends Component {
  // constructor(){
  //   super();
  //   this.state = {
  //       hotaldata: HOTAL_DATA,
  //       comments:  COMMENTS,
  //       selected_Hotal: null
  //     };
  // }
  onHotalSelect(hotalId){
    this.setState({ selected_Hotal: hotalId });
  }
  // componentDidMount(){
  //   this.props.fetch_Hotals()
  // }
  render() {
      const HomePage = () =>{
        return(        
            <Home
            hotal={this.props.hotaldata.filter((hotal) => hotal.featured)[0]}
            />
          );
      }
      const hotalWithId =({match}) =>{
      return(
        <HotalDetail
        hotal={
          this.props.hotaldata.filter(
            hotal => hotal.id === parseInt(match.params.hotalId, 10)
          )[0]
        }
        // hotaldata={
        //   this.props.hotaldata.filter(
        //     hotal => hotal.id === parseInt(match.params.hotalId, 10)
        //   )[1]
        // }
        // isLoading={this.props.hotaldata.isLoading}
        // errMess={this.props.hotaldata.errMess}
        comments={this.props.comments.filter(
        comment => comment.hotalId === parseInt(match.params.hotalId, 10)
        )}
        commentsErrMess={this.props.comments.errMess}
        // addComment={this.props.addComment}
        // postComment={this.props.postComment}
        />
        );
    }
    return (
      <div>
      <NavBar />
      <Switch>
        <Route path="/" exact component={HomePage}/>
        <Route
          path="/hotels"
          exact
          component={
            () => (
              <Menu_Hotal
              hotaldata={this.props.hotaldata}
              onClick={(hotalId) => this.onHotalSelect(hotalId)}
              />
              )
            }
            />
            <Route path="/hotels/:hotalId" exact component={hotalWithId}/> 
            <Route path="/aboutus" exact component={About} />
            <Route path="/login" exact component={Login} />
            <Route path="/signIn" exact component={SignIn} />
            <Route path="/contact" exact component={Contact} />
            <Redirect to="/" exact />
            </Switch>
            <Footer />
            </div>
    );
  }
}
export default withRouter(connect(mapStateToProp)(Main));
//export default Main;
      


