import * as actionTypes from "./actionTypes";
// import { HOTAL_DATA } from "../Shared/hotalData";
export const postComment = (hotalId, rating, comment, author) => ({
  type: actionTypes.ADD_COMMENT,
  payload: {
    hotalId,
    rating,
    comment,
    author,
  },
});

// export const fetch_Hotals = () => (dispatch) =>{
//   dispatch(actionTypes.HOTAL_LOADING(true));

//   setTimeout( ()=>{
//     dispatch(actionTypes.ADD_HOTALS(HOTAL_DATA))
//   },2000)

// }

// export const HOTAL_LOADING = () =>({
//   type:actionTypes.HOTAL_LOADING
// });
// export const hotal_Failed = (errmess) =>({
// type:actionTypes.HOTALS_FAILED,
// payload:errmess
// });

// export const add_HOTALS = (hotals) =>({
// type:actionTypes.ADD_HOTALS,
// payload:hotals
// });


