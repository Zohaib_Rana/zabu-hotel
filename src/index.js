import React from "react";
import ReactDOM from "react-dom";
import "./CSS_Work/class.css";
import "./CSS_Work/id_.css";
import App from "./components/App";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-social";
import "font-awesome/css/font-awesome.css";
import "bootstrap-social/bootstrap-social.css";
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
