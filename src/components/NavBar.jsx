import React, { Component } from "react";
import {Modal,Form,FormGroup,Label,Input,Button,ModalBody, 
  Nav, Navbar, NavbarBrand, NavbarToggler, Collapse, NavItem } from 'reactstrap';
import { NavLink } from 'react-router-dom';
class NavBar extends Component {
  constructor() {
    super();
    this.state = {
      isNavOpen: false,
      isModalOpen: false,
    };
    this.toggleNav = this.toggleNav.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }
  toggleNav() {
    this.setState({
      isNavOpen: !this.state.isNavOpen,
    });
  }
  toggleModal = () => {
    this.setState({
      isModalOpen: !this.state.isModalOpen,
    });
  };
  handleLogin = (event) => {
    this.toggleModal();
    alert(
      "Username: " +
        this.username.value +
        " Password: " +
        this.password.value +
        " Remember: " +
        this.remember.checked
    );
    event.preventDefault();
  };
  render() {
    return (
      <div >
    <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
          <li className="back_col" toggle={this.toggleModal}>
            Login
          </li>
          <ModalBody>
            <Form onSubmit={this.handleLogin}>
              <FormGroup>
                <Label htmlFor="username">Username</Label>
                <Input
                  required
                  type="text"
                  id="username"
                  name="username"
                  innerRef={(input) => (this.username = input)}
                />
              </FormGroup>
              <FormGroup>
                <Label htmlFor="password">Password</Label>
                <Input
                  required
                  type="password"
                  id="password"
                  name="password"
                  innerRef={(input) => (this.password = input)}
                />
              </FormGroup>
              <FormGroup check>
                <Label check>
                  <Input
                    type="checkbox"
                    name="remember"
                    innerRef={(input) => (this.remember = input)}
                  />
                  Remember me
                </Label>
              </FormGroup>
              <Button type="submit" value="submit" color="primary">
                Login
              </Button>
            </Form>
          </ModalBody>
        </Modal>
   
   
    <Navbar dark expand="md"  >
      <div className="container">
          <NavbarToggler onClick={this.toggleNav} />
          <Nav className="ml-auto" bg-primary>
          <NavbarBrand ><NavLink to="/"> 
            {/* <img src='assests/images/rana.jpg' 
            height="30" width="41" alt='Zabu Hotals' /> */}
             <span className="ml-2"><strong>Zabu Hotels</strong></span>
            </NavLink>
            </NavbarBrand>
            </Nav>
          <Collapse isOpen={this.state.isNavOpen} navbar>
              <Nav navbar className=" mr-2 " >
              <NavItem>
                  <NavLink className="nav-link "  to='/hotels'>
                    <span className="fa fa-home fa-md"></span> 
                    <strong>Hotel's</strong></NavLink>
              </NavItem>
              </Nav>
              <Nav className=" mr-2">
              <NavItem>
                  <NavLink className="nav-link" to='/aboutus'>
                    <span className="fa fa-info fa-md"></span>
                    <strong>About Us</strong> </NavLink>
              </NavItem>
              </Nav>
              <Nav className="">
              <NavItem>
                <NavLink className="nav-link"to='/contact'>
                   <span className="fa fa-address-book fa-md"></span>
                   <strong>Contact Us</strong></NavLink>
              </NavItem>
            </Nav>
            <Nav className="ml-auto ">
              <NavItem>
                <NavLink
                   onClick={this.toggleModal} 
                   className="nav-link"  to='/login'>
                    <span className="fa fa-sign-in fa-md">
                    </span><strong>Login</strong></NavLink>
              </NavItem>
              </Nav>
              <Nav>
              <NavItem className=" mr-2 " >
                <NavLink className="nav-link" to='/signin'>
                    <span className="fa fa-sign-in fa-md">
                      </span><strong>SignIn</strong></NavLink>
               </NavItem>
              </Nav>
       </Collapse>
      </div>
    </Navbar>      
  </div>
    );
  }
}

export default NavBar;

