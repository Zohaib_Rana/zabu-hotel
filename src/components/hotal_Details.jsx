import React, { Component } from "react";
import {
  Card,CardImg,CardText,CardBody,CardTitle,Modal, ModalHeader,ModalBody,
  Breadcrumb,BreadcrumbItem,Button,Row,Col,Label} from "reactstrap";
import { Control, LocalForm, Errors } from "react-redux-form";
import { Link } from "react-router-dom";
import { Loading } from "./LoadingComponent";
import { FadeTransform, Fade, Stagger } from "react-animation-components";

const required = val => val && val.length;
const maxLength = len => val => !val || val.length <= len;
const minLength = len => val => val && val.length >= len;

class CommentForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false
    };
    this.toggleModal = this.toggleModal.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  toggleModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  }

  handleSubmit(values) {
    this.toggleModal();
    this.props.postComment(
      this.props.hotalId,
      values.rating,
      values.author,
      values.comment
    );
  }

  render() {
    return (
      <div>
        <Button outline onClick={this.toggleModal}>
          <span className="fa fa-pencil" /> Submit Comment
        </Button>
        <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
          <ModalBody>
            <LocalForm onSubmit={this.handleSubmit}>
              <Row className="form-group">
                <Label htmlFor="rating" md={12}>
                  Rating
                </Label>
                <Col md={{ size: 12 }}>
                  <Control.select
                    model=".rating"
                    name="rating"
                    className="form-control"
                  >
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </Control.select>
                </Col>
              </Row>
              <Row className="form-group">
                <Label htmlFor="author" md={12}>
                  Your Name
                </Label>
                <Col md={12}>
                  <Control.text
                    model=".author"
                    id="author"
                    name="author"
                    placeholder="Your Name"
                    className="form-control"
                    validators={{
                      required,
                      minLength: minLength(3),
                      maxLength: maxLength(15)
                    }}
                  />
                  <Errors
                    className="text-danger"
                    model=".author"
                    show="touched"
                    messages={{
                      required: "Required",
                      minLength: "Must be greater than 2 characters",
                      maxLength: "Must be 15 characters or less"
                    }}
                  />
                </Col>
              </Row>
              <Row className="form-group">
                <Label htmlFor="comment" md={12}>
                  Comment
                </Label>
                <Col md={12}>
                  <Control.textarea
                    model=".comment"
                    id="comment"
                    name="comment"
                    rows={5}
                    className="form-control"
                  />
                </Col>
              </Row>
              <Button type="submit" value="submit" color="primary">
                Submit
              </Button>
            </LocalForm>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}


function RenderHotal({ hotal }) {
  return (
    <div className="col-12 col-md-5 m-1">
      <FadeTransform
        in
        transformProps={{
          exitTransform: "scale(0.5) translateY(-50%)"
        }}
      >
        <Card>
          <CardImg top src={ hotal.imgSrc} alt={hotal.hotal_Name} />
          <CardBody>
            {/* <CardTitle>{hotal.hotal_Name}</CardTitle> */}
            <CardText>{hotal.Hotal_Description}</CardText>
          </CardBody>
        </Card>
      </FadeTransform>
    </div>
  );
}

// function RenderDetails({ hotaldata }) {
//   return (
//     <div className="col-12 col-md-5 m-1">
//       <FadeTransform
//         in
//         transformProps={{
//           exitTransform: "scale(0.5) translateY(-50%)"
//         }}
//       >
//         <Card>
//           <CardImg top src={ hotaldata.imgSrc} alt={hotaldata.hotal_Name} />
//           <CardBody>
//             {/* <CardTitle>{hotaldata.hotal_Name}</CardTitle> */}
//             <CardText>{hotaldata.Hotal_Description}</CardText>
//           </CardBody>
//         </Card>
//       </FadeTransform>
//     </div>
//   );
// }
function RenderComments({ comments, postComment, hotalId }) {
  if (comments != null) {
    return (
      <div className="col-12 col-md-5 m-1">
        <h4>Comments</h4>
        <Stagger in>
          {comments.map(comment => {
            return (
              <Fade in key={comment.id}>
                <li key={comment.id}>
                  <p>{comment.comment}</p>
                  <p>
                    -- {comment.author} ,{" "}
                    {new Intl.DateTimeFormat("en-US", {
                      year: "numeric",
                      month: "short",
                      day: "2-digit"
                    }).format(new Date(Date.parse(comment.date)))}
                  </p>
                </li>
              </Fade>
            );
          })}
        </Stagger>
        <CommentForm hotalId={hotalId} postComment={postComment} />
      </div>
    );
  } else return <div />;
}

const DishDetail = props => {
  if (props.isLoading) {
    return (
      <div className="container">
        <div className="row">
          <Loading />
        </div>
      </div>
    );
  } else if (props.errMess) {
    return (
      <div className="container">
        <div className="row">
          <h4>{props.errMess}</h4>
        </div>
      </div>
    );
  } else if (props.hotal != null)
    return (
      <div className="container">
        <div className="row">
          <Breadcrumb>
            <BreadcrumbItem>
              <Link to="/hotels">Hotel's</Link>
            </BreadcrumbItem>
            <BreadcrumbItem active>{props.hotal.hotal_Name}</BreadcrumbItem>
          </Breadcrumb>
          <div className="col-12">
            <h3>{props.hotal.hotal_Name}</h3>
            <hr />
          </div>
        </div>
        <div className="row">
          <RenderHotal hotal={props.hotal} />
          <RenderComments
            comments={props.comments}
            postComment={props.postComment}
            hotalId={props.hotal.id}
          />
          {/* <RenderDetails hotaldata={props.hotaldata}/> */}
        </div>
      </div>
    );
};

export default DishDetail;






