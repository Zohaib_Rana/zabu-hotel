import React from 'react';
import { Card, CardImg, CardText, CardBody,CardTitle,Breadcrumb,BreadcrumbItem} from 'reactstrap';
import {Link} from "react-router-dom";
import {Loading} from "../components/LoadingComponent";
import { UncontrolledCarousel } from 'reactstrap';
import { FadeTransform} from "react-animation-components";
const items = [
  {
    src: 'assests/images/01 isl.jpg',  
    altText: 'Islamabad Hotal',
    caption: 'Ishu Hotal',
    // header: 'Ishu Hotal',
    key: '1'
  },
  {
    src: 'assests/images/01.webp',
    altText: 'Slide 2',
    caption: "Karachi Hotal's",
    // header: 'Slide 2 Header',
    key: '2'
  },
  {
    src: 'assests/images/03 isl.png',
    altText: 'Slide 3',
    caption: "Lahore Hotel's",
    // header: 'Slide 3 Header',
    key: '3'
  }
];
const Example = () => <UncontrolledCarousel items={items} />;
    
function RenderCard({item,isLoading,errMess,hotal}) {    
          
  if(isLoading){
            return(
              <Loading/>
            );
          }
          else if(errMess){
            return(
            <h4>{errMess}</h4>
            );
          }
        // else
          return(
                <div  className="col-12 col-md m-1" >
                  <FadeTransform
                      in
                      transformProps={{
                      exitTransform: "scale(0.5) translateY(-50%)"
                      }}
                  >
                <Card 
                 >
                  <CardBody>
                    <Link to={`/hotels/${hotal.id}`}>     
                      <CardImg width="100%" src={hotal.imgSrc} alt={hotal.hotal_Title}/>
                    </Link>
                    <CardTitle>{hotal.hotal_Title}</CardTitle> 
                    <CardText>{hotal.Hotal_Description}</CardText> 
                  </CardBody>
                </Card>
                </FadeTransform>            
            </div>
          );
      }

      function Home(props) {
          return(
              <div className="container">
                  <div className="row">
                <Breadcrumb>
              <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
              <BreadcrumbItem><Link to="/hotels">Hotel's</Link></BreadcrumbItem>
              <BreadcrumbItem active>Home</BreadcrumbItem>
              </Breadcrumb>
              <div className="col-12">
              <h3>Home Page here that</h3>
              <hr />
              </div>
              </div>
              <div className="col-12 col-md m-1">
                {/* <div className="row"> */}
                  <Link to="/hotals">
                    <Example className="caru_Style"/>
                  </Link>                
                {/* </div> */}
              </div>
                  <div className="row align-items-start">
                      <div className="col-12 col-md m-1">
                          <RenderCard hotal={props.hotal}
                          isLoading={props.hotalLoading}
                          errMess={props.hotalErrMess} />
                      </div>
                      <div className="col-12 col-md m-1">
                          <RenderCard hotal={props.hotal}
                           isLoading={props.hotalLoading}
                           errMess={props.hotalErrMess} />
                      </div>
                      <div className="col-12 col-md m-1">
                          <RenderCard hotal={props.hotal}
                           isLoading={props.hotalLoading}
                          errMess={props.hotalErrMess} />
                      </div>
                  </div>
          
            
          <div role="tabpanel" class="tab-pane fade show active" id="peter">
              <h3>Peter Pan <small>Chief Epicurious Officer</small></h3>
            <p> </p>
          <div class="tab-content">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a class="nav-link active" href="#peter"
                    role="tab" data-toggle="tab">Peter Pan, CEO</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#danny" role="tab"
                    data-toggle="tab">Danny Witherspoon, CFO</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#agumbe"role="tab"
                    data-toggle="tab">Agumbe Tang, CTO</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#alberto" role="tab"
                    data-toggle="tab">Alberto Somayya, Exec. Chef</a>
                </li>
            </ul>
        </div>
      </div>
     
              <div className="container">
                <div className="jumbotron mt-5">
                  <h1>Zabu Hotal's</h1>
                  <h6>Talk To The World</h6>
                  <p>Looking Hotal's through Zabu </p>
                  <h6>We know the living standard's all over the world</h6>
                  <button className="badge badge-primary md mt-2 bg-white">
                    <Link to="/Hotels">Book Now !</Link>
                  </button>
                </div>
              </div> 
              </div>
          );
      }
      export default Home;
      //  <div className="container col-12 col-sm-12">
      //           <div className="jumbotron  mt-3">
      //           <div className="row row-header ">
      //             <div className="">
      //               <h1>
      //                 <strong>Zabu Hotal's</strong>
      //               </h1>
      //               <p>
      //                 <em>
      //                   We take inspiration from the World's best cuisines, and create
      //                   a unique fusion experience. Our lipsmacking creations will
      //                   tickle your culinary senses!
      //                 </em>
      //               </p>
      //             </div>
      //             </div>
      //           </div>
      //         </div> 
                  

              