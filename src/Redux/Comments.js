import { COMMENTS } from "../Shared/CommentsData";
// import { HOTAL_DATA } from "../Shared/hotalData";
import * as actionTypes from "./actionTypes";
export const Comments = (state = COMMENTS, action) => { 
  switch (action.type) {        
    case actionTypes.ADD_COMMENT:
      var comment = action.payload;
      comment.id = state.length;
      comment.date = new Date().toISOString();
      return state.concat(comment);
    default:
      return state;
  }
};

// import { COMMENTS } from '../Shared/CommentsData';

// export const Comments = (state = COMMENTS, action) => {
//     switch (action.type) {

//         default:
//           return state;
//       }
// };